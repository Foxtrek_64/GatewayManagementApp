﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GatewayManagementApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private string mode;
        public string Mode
        {
            get
            {
                if (string.IsNullOrEmpty(mode))
                {
                    mode = GetMode();
                }
                return mode;
            }
            set
            {
                mode = value;
            }
        }

        BitmapSource shield;
        BitmapSource Shield
        {
            get
            {
                if (shield == null)
                {
                    shield = GetAdminShield.GetShield();
                }
                return shield;
            }
        }

        private string GetMode()
        {
            return "Pizza";
        }

        public MainWindow()
        {
            InitializeComponent();
        }
    }
}
